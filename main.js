/*Экранирование нужно для того чтобы превращять специальные символы в обычную строку */




function createNewUser() {
    return {
        firstName: prompt("Please enter your name"),
        lastName: prompt("Please enter your last name"),
        birthday: prompt("Please enter your date of birth", "dd.mm.yyyy"),
        getAge: function () {
            return new Date().getFullYear() - this.birthday.slice(6, 10);
        },
        getPassword: function () {
            return this.firstName.toUpperCase().charAt(0) + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        },
    }
}
const newUser = createNewUser();
console.log(newUser);
console.log("User age - " + newUser.getAge());
console.log("User login is - " + newUser.getPassword());



